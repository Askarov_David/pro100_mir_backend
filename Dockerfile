FROM keymetrics/pm2:8-jessie

RUN npm install pm2 -g
RUN mkdir /var/pro100_mir

COPY . /var/pro100_mir

WORKDIR /var/pro100_mir
RUN npm install
RUN npm run build
CMD [ "pm2-runtime", "start", "pm2.json" ]
