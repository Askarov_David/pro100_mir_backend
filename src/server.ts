import app from './app';
const config = require('./config');

app.listen(config.get('port'), () => {
    console.log('--- Server ready ---');
});