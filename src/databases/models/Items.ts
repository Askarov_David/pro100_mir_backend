import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const items = sequelize.define('items', {
        title: { type: Sequelize.TEXT, allowNull: false },
        short_title: { type: Sequelize.TEXT, allowNull: false },
        article: { type: Sequelize.BIGINT, allowNull: false },
        fk_measurement_units_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'schedules',
                key: 'id'
            },
            allowNull: false,
        },
        barcode: { type: Sequelize.TEXT, allowNull: false },
        description: { type: Sequelize.TEXT, allowNull: false },
        price_by_unit: { type: Sequelize.DOUBLE, allowNull: false },
        photo: { type: Sequelize.TEXT, allowNull: false },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return items;
};
