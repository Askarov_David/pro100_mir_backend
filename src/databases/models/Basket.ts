import Sequelize from 'sequelize';

module.exports = (sequelize: any) => {
    const Basket = sequelize.define('basket', {
        type: {
            type: Sequelize.STRING,
            allowNull: false
        },
        data: {
            type: Sequelize.JSONB,
            allowNull: false
        },
        date: {
            type: Sequelize.DATE,
            default: new  Date()
        }
    }, {
        freezeTableName: true,
    });
    return Basket;
};
