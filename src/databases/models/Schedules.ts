import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const schedules = sequelize.define('schedules', {
        cron_string: { type: Sequelize.TEXT, allowNull: false },
        start: { type: Sequelize.DATE, allowNull: false },
        end: { type: Sequelize.DATE, allowNull: false },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return schedules;
};