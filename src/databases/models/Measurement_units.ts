import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const measurementUnits = sequelize.define('measurement_units', {
        short_title: { type: Sequelize.STRING(50), allowNull: false },
        title: { type: Sequelize.TEXT, allowNull: false }
    }, {
        freezeTableName: true,
    });

    return measurementUnits;
};