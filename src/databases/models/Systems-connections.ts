import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const systemsConnections = sequelize.define('systems-connections', {
        title: { type: Sequelize.STRING(150), allowNull: false },
        secret: { type: Sequelize.TEXT, allowNull: false },
        connection_info: { type: Sequelize.JSON, allowNull: false },
        fk_system_types_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'system_types',
                key: 'id'
            },
            allowNull: false,
        },
        fk_schedules_types_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'schedules',
                key: 'id'
            },
            allowNull: false,
        },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return systemsConnections;
};
