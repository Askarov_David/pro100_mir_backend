import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const ordersContent = sequelize.define('orders_content', {
        title: { type: Sequelize.TEXT, allowNull: false },
        fk_items_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'items',
                key: 'id'
            },
            allowNull: false,
        },
        quantity: { type: Sequelize.DOUBLE, allowNull: false },
        fk_orders_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'orders',
                key: 'id'
            },
            allowNull: false,
        },
        cost: { type: Sequelize.DOUBLE, allowNull: false },
        discount: { type: Sequelize.DOUBLE, allowNull: false },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
        }, {
        freezeTableName: true,
    });

    return ordersContent;
};