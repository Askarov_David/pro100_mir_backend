import Sequelize from 'sequelize';
import * as md5 from 'md5';
import app from '../../app';
import { User } from '../../shared';

module.exports = (sequelize: any) => {
    const Users = sequelize.define('users', {
        username: { type: Sequelize.STRING(60), allowNull: false },
        phone: {
            type: Sequelize.STRING(16),
            allowNull: false,
            validate : {
                not: ["[a-z]",'i'],
                notEmpty: true,
            }
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true,
                notEmpty: true,
                len: [1,100]
            }
        },
        password : {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        key: { type: Sequelize.STRING, allowNull: false },
        is_block: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        fk_roles_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'roles',
                key: 'id'
            },
            allowNull: false,
        },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        },
        action: {
            type: Sequelize.VIRTUAL
        }
    }, {
        freezeTableName: true,
        indexes: [{unique: true, fields: ['email']}],
        hooks: {
            beforeFind: async (instance) => {
                if(instance.where.password) {
                    instance.where.password = md5(instance.where.password);
                }
            },
            beforeSave: async (instance) => {
                try {
                    if(instance.action) {
                        /**
                         * TODO create action for user
                         */
                        await app['db'].userAction.create(instance.action );
                    }
                    instance.password = md5(instance.password);
                } catch (error) {
                    return new Error('500');
                }
            },
            beforeDestroy: async (instance) => {
                try {
                    const user = new User(
                        instance.dataValues.username,
                        instance.dataValues.email,
                        instance.dataValues.phone,
                        instance.dataValues.password,
                        instance.dataValues.key,
                        instance.dataValues.is_block,
                        instance.dataValues.fk_roles_id,
                        instance.dataValues.fk_partners_id,
                        instance.dataValues.id);

                    await app['db'].basket.create({
                        type: 'users',
                        data: user,
                        date: new Date()
                    });
                } catch (error) {
                    return new Error('500');
                }
            }
        }
    });

    return Users;
};


