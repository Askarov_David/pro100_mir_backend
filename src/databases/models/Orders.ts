import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const orders = sequelize.define('orders', {
        date: { type: Sequelize.DATE, allowNull: false },
        full_name: { type: Sequelize.TEXT, allowNull: false },
        cost: { type: Sequelize.DOUBLE, allowNull: false },
        fk_storages_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'storages',
                key: 'id'
            },
            allowNull: false,
        },
        discount: { type: Sequelize.DOUBLE, allowNull: false },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return orders;
};