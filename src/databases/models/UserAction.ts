import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const userAction = sequelize.define('user_action', {
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'users',
                key: 'id'
            },
            allowNull: false
        },
        section: { type: Sequelize.STRING, allowNull: false },
        date: { type: Sequelize.DATE, allowNull: false },
        action: { type: Sequelize.STRING, allowNull: false }
    }, {
        freezeTableName: true,
    });

    return userAction;
};
