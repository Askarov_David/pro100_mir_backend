import Sequelize from 'sequelize';
import app from "../../app";
import { Partner } from "../../shared";

module.exports = (sequelize: Sequelize) => {
    const partners = sequelize.define('partners', {
        title:          { type: Sequelize.STRING(100), allowNull: false },
        logo:           { type: Sequelize.TEXT, allowNull: false },
        address:        { type: Sequelize.STRING(50) },
        contact_person: { type: Sequelize.STRING(50) }
    }, {
        freezeTableName: true,
        hooks: {
            beforeDestroy: (instance) => {
                const partner = new Partner(
                    instance.dataValues.title,
                    instance.dataValues.logo,
                    instance.dataValues.address,
                    instance.dataValues.contact_person,
                    instance.dataValues.id);

                app['db'].basket.create({
                    type: 'partners',
                    data: partner,
                    date: new Date()
                });
            },
            beforeSave: async (instance) => {
                try {
                    if(instance.action) {
                        /**
                         * TODO create action for partners
                         */
                        await app['db'].userAction.create(instance.action );
                    }
                } catch (error) {
                    return new Error('500');
                }
            }
        }
    });

    return partners;
};

