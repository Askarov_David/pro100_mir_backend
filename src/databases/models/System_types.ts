import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const systemTypes = sequelize.define('system_types', {
        title: { type: Sequelize.TEXT, allowNull: false },
        connection_fields: { type: Sequelize.JSON, allowNull: false }
    }, {
        freezeTableName: true,
    });

    return systemTypes;
};