import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const roles = sequelize.define('roles', {
        title: { type: Sequelize.STRING(50), allowNull: false },
        permission: { type: Sequelize.INTEGER, allowNull: false }
    }, {
        freezeTableName: true,
    });

    return roles;
};


