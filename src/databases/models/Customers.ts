import Sequelize from 'sequelize';
// import bcrypt from 'bcrypt';

module.exports = (sequelize: Sequelize) => {
    const customers = sequelize.define('customers', {
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true,
                notEmpty: true,
                len: [1,100]
            }
        },
        phone: {
            type: Sequelize.STRING(16),
            allowNull: false,
            validate : {
                not: ["[a-z]",'i'],
                notEmpty: true,
            }
        },
        address: { type: Sequelize.TEXT, allowNull: false },
        full_name: { type: Sequelize.TEXT, allowNull: false },
        social_accounts: { type: Sequelize.JSON, allowNull: false },
        messengers_accounts: { type: Sequelize.JSON, allowNull: false },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    }, {
        freezeTableName: true,
    });

    // customers.beforeCreate((customers: any, options: any) => {
    //     customers.email = customers.email.toLowerCase();
    //     if (customers.password) {
    //         bcrypt.hash(customers.get('password'), 10, (err: any, hash: any) => {
    //             if (err) throw new Error("Error bcrypt password");
    //             customers.set('password', hash);
    //             customers.save();
    //         });
    //     }
    // });

    return customers;
};