import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const remnants = sequelize.define('remnants', {
        quantity: { type: Sequelize.DOUBLE, allowNull: false },
        fk_items_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'items',
                key: 'id'
            },
            allowNull: false,
        },
        fk_systems_connections_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'systems-connections',
                key: 'id'
            },
            allowNull: false,
        },
        fk_storages_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'storages',
                key: 'id'
            },
            allowNull: false,
        },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return remnants;
};