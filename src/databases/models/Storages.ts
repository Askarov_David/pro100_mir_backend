import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const storages = sequelize.define('storages', {
        title: { type: Sequelize.TEXT, allowNull: false },
        fk_partners_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'partners',
                key: 'id'
            },
            allowNull: false,
        }
    }, {
        freezeTableName: true,
    });

    return storages;
};