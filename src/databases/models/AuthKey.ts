import Sequelize from 'sequelize';
import app from '../../app';

module.exports = (sequelize: Sequelize) => {
    const authKey = sequelize.define('auth_key', {
        user_id: { type: Sequelize.INTEGER, allowNull: false },
        secret: { type: Sequelize.STRING, allowNull: false },
        attempts: { type: Sequelize.INTEGER, default: 0 },
        expire: { type: Sequelize.DATE, allowNull: false },
        timeBan: { type: Sequelize.DATE },
        token: { type: Sequelize.TEXT, allowNull: false }
    }, {
        freezeTableName: true,
        hooks: {
            beforeCreate: async (instance) => {
                /**
                 * Check exist auth key for user
                 */
                const keyOld  = await app['db'].authKey.findOne({
                    where: { user_id: instance.user_id }
                });

                if(keyOld) {
                     return await keyOld.destroy();
                }
            }
        }
    });

    return authKey;
};


