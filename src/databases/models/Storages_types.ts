import Sequelize from 'sequelize';

module.exports = (sequelize: Sequelize) => {
    const storagesTypes = sequelize.define('storages-types', {
            title: { type: Sequelize.TEXT, allowNull: false }
        }, {
        freezeTableName: true,
    });

    return storagesTypes;
};