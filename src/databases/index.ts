import { Sequelize } from 'sequelize';
const config = require('../config');

export class SequelizeDB {
    private sequelize: Sequelize;

    constructor() {}

    public init() {
        let db;

        this.sequelize = new Sequelize(config.get('db:name'), config.get('db:username'), config.get('db:password'), {
            host: config.get('db:host'),
            port: config.get('db:port'),
            dialect: 'postgres'
        });

        db = {
            users: this.sequelize.import('./models/Users'),
            roles: this.sequelize.import('./models/Roles'),
            partners: this.sequelize.import('./models/Partners'),
            systemsConnections: this.sequelize.import('./models/Systems-connections'),
            systemTypes: this.sequelize.import('./models/System_types'),
            schedules: this.sequelize.import('./models/Schedules'),
            storagesTypes: this.sequelize.import('./models/Storages_types'),
            storages: this.sequelize.import('./models/Storages'),
            remnants: this.sequelize.import('./models/Remnants'),
            items: this.sequelize.import('./models/Items'),
            measurement_Units: this.sequelize.import('./models/Measurement_units'),
            orders: this.sequelize.import('./models/Orders'),
            ordersContent: this.sequelize.import('./models/Orders_content'),
            customers: this.sequelize.import('./models/Customers'),
            authKey: this.sequelize.import('./models/AuthKey'),
            userAction: this.sequelize.import('./models/UserAction'),
            basket: this.sequelize.import('./models/Basket')
        };

        /**
         * References for Users
         */
        db.users.belongsTo(db.roles, { foreignKey: 'fk_roles_id' });
        db.users.belongsTo(db.partners, {foreignKey: 'fk_partners_id'});

        /**
         * References for Auth Key
         */
        db.authKey.belongsTo(db.users, { foreignKey: 'user_id' });

        /**
         * References for User Action
         */

        db.userAction.belongsTo(db.users, { foreignKey: 'user_id' });

        Object.keys(db).forEach(modelName => {
            if (db[modelName].associate) {
                db[modelName].associate(db);
            }
        });

        /**
         * For testing sequelize
         */
        this.sequelize.authenticate()
            .then(() => {
                console.log('Connection to DB has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });

        db.sequelize = this.sequelize;
        return this.sequelize.sync({})
            .then(() => {
                console.log('LOAD TABLE');
                return Promise.resolve(db)
            });
    }

}
