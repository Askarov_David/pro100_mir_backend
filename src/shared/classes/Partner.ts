export class Partner {
    public id : number;
    public title : string;
    public logo: string;
    public address: string;
    public contact_person: string;

    constructor(title: string,
                logo: string,
                address: string,
                contact_person: string,
                id?: number,) {
        if(id)  this.id = id;
        this.title = title;
        this.logo = logo;
        this.address = address;
        this.contact_person = contact_person;
    }
}