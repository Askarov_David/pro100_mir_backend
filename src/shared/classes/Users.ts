export class User {
    public id : number;
    public username : string;
    public phone: string;
    public email: string;
    public password: string;
    public key: string;
    public is_block: boolean;
    public fk_roles_id: number;
    public fk_partners_id: number;

    constructor(username: string,
                email: string,
                phone: string,
                password: string,
                key: string,
                is_block: boolean,
                fk_roles_id: number,
                fk_partners_id: number,
                id?: number,) {

        if(id)  this.id = id;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.key = key;
        this.password = password;
        this.is_block = is_block;
        this.fk_roles_id = fk_roles_id;
        this.fk_partners_id = fk_partners_id;
    }
}