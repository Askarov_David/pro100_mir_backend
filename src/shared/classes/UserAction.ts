export class UserAction {
    public section : string;
    public date: Date;
    public user_id: number;
    public action: string;

    constructor(user_id: number, action: string, section: string) {
        this.user_id = user_id;
        this.action = action;
        this.section = section;
        this.date = new Date();
    }
}