class GenerateAuthKey {
    constructor() {}

    public getKey(): string {
        let key='';
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(let i = 0; i < 8; i++) {
            key += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return key
    }
}

export default new GenerateAuthKey();

