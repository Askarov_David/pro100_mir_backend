import * as nodemailer from 'nodemailer'
const config = require('../config');

class SendEmail {

    private transporter;

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.get('mail:user'),
                pass: config.get('mail:password'),
            }
        });
    }

    public sendEmail(to: string, text: string, subject: string, html ?: string) {
        const mailOptions = {
            from: config.get('mail:from'),
            to,
            subject,
            text,
            html
        };

        this.transporter.sendMail(mailOptions, (error, info) => {
            if(error) {
                console.log('error : nodemailer : ', error)
            }
        })
    }
}

export default new SendEmail();