import * as express from "express";
import * as ctrl from '../controllers';

export class BasketRoute {
    public routes: express.Router;

    constructor() {
        this.routes = express.Router();
        const basketCtrl = ctrl.BasketController;

        this.routes.get('/', basketCtrl.getAllBasket);
        this.routes.get('/recovery/:profileId', basketCtrl.recoveryProfile);
    }
}