const swaggerUi = require('swagger-ui-express');
import * as express from "express";
import { Request, Response, NextFunction } from "express";
import { ApiDock } from "../api.docs";
import app from '../app';

export class ApiDocumentRoute {
    public routes: express.Router;

    constructor() {
        this.init();
    }

    init() {
        this.routes = express.Router();
        const apiDoc = new ApiDock();

        this.routes.get('/admin', (req: Request, res: Response, next: NextFunction) => {
            app.use('/doc/admin', swaggerUi.serve, swaggerUi.setup(apiDoc.swaggerDocumentAdmin));
            next();
        });

        this.routes.get('/chatbot', (req: Request, res: Response, next: NextFunction) => {
            app.use('/doc/chatbot', swaggerUi.serve, swaggerUi.setup(apiDoc.swaggerDocumentChatbot));
            next();
        });
    }
}