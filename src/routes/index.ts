import { ApiDocumentRoute }  from './api.doc';
import { UserRoute } from './user';
import { PartnersRoute } from './partners';
import { BasketRoute } from './basket';

export {
    ApiDocumentRoute,
    UserRoute,
    PartnersRoute,
    BasketRoute
}