import * as express from "express";
import * as ctrl from '../controllers';

export class PartnersRoute {
    public routes: express.Router;

    constructor() {
        this.routes = express.Router();
        const partnerCtrl = ctrl.PartnersController;

        this.routes.get('/', partnerCtrl.getAllPartners);
        this.routes.post('/', partnerCtrl.createPartner);

        this.routes.get('/:partnerId', partnerCtrl.getPartner);
        this.routes.put('/:partnerId', partnerCtrl.updatePartner);
        this.routes.delete('/:partnerId', partnerCtrl.deletePartner);

        this.routes.get('/:partnerId/users', partnerCtrl.getUsersPartner);
    }
}