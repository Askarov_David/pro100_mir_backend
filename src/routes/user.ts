import * as express from "express";
import * as ctrl from '../controllers';
import * as passport from 'passport';
import { isAuthMiddleware } from '../middleware';

export class UserRoute {
    public routes: express.Router;

    constructor() {
        this.init();
    }

    private init() {
        this.routes = express.Router();
        const authCtrl = ctrl.AuthorizationController;
        const userCtrl = ctrl.UserController;

        this.routes.post('/auth', passport.authenticate('local', { session: false }), authCtrl.authorization);
        this.routes.post('/auth/confirmation', authCtrl.authConfirmation);
        this.routes.post('/auth/recovery', authCtrl.recoveryAccount);
        this.routes.put('/auth/reset', isAuthMiddleware.isAuth, authCtrl.changePassword);

        this.routes.get('/my-profile', isAuthMiddleware.isAuth, userCtrl.getProfile);
        this.routes.put('/my-profile', isAuthMiddleware.isAuth, userCtrl.updateProfile);
        this.routes.get('/profile/search', isAuthMiddleware.isAuth, userCtrl.searchThroughProfile);
        this.routes.post('/profile/create', isAuthMiddleware.isAuth, userCtrl.createProfile);
        this.routes.put('/profile/:userId', isAuthMiddleware.isAuth, userCtrl.updateProfileById);
        this.routes.delete('/profile/remove/:userId', isAuthMiddleware.isAuth, userCtrl.deleteProfileById);
        this.routes.delete('/profiles/remove', isAuthMiddleware.isAuth, userCtrl.deleteProfiles);

        this.routes.get('/action/:userId', isAuthMiddleware.isAuth, userCtrl.getActionUser);
        this.routes.put('/action/:actionId', isAuthMiddleware.isAuth, userCtrl.editActionUserById);
        this.routes.delete('/action/:actionId', isAuthMiddleware.isAuth, userCtrl.removeActionById);
    }
}