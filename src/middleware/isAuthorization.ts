import { Request, Response, NextFunction } from 'express'
import app from '../app';

const jwt = require('jwt-simple');
const config = require('../config');

class IsAuthorization {
    constructor() {}

    public async isAuth(req: Request, res: Response, next: NextFunction) {
        if(!req.headers['x-guide-key']) {
            return next(new Error('401'));
        }
        const obj = jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
        const user = await app['db'].users.findOne({
            where: {
                id: obj.userId,
                fk_partners_id: obj.partnerId
            }
        });

        if(!user) {
            return next(new Error('498'));
        }

        switch(obj.permission) {
            case 1:
                return next();
            case 2:
                req.query['partnerId'] = obj.partnerId;
                return next();
            case 3:
                req.query['partnerId'] = obj.partnerId;
                return next();
            case 4:
                if (req.method === 'GET') {
                    return next();
                } else {
                    return next(new Error('403'));
                }
            default:
                return next(new Error('403'));
        }
    }
}

export default new IsAuthorization();