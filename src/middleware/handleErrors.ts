import { Request, Response, NextFunction } from "express";

const errStatus = {
    500: 'Ошибка сервера',
    401: 'Пользователь не авторизованный',
    404: 'Указанный ресурс не найден',
    400: 'Проверьте запрос отправленный серверу',
    403: 'Вы не имеете достаточно прав',
    498: 'Срок действия токена истек'
};

class HandlerErrors {
    constructor() {}

    public errorHandler(error: Error, req: Request, res: Response, next: NextFunction) {
        if(isNaN(parseFloat(error.message))) {
            error.message = '500';
        }
        res.status(parseInt(error.message));
        res.json({
            status: parseInt(error.message),
            message: errStatus[error.message]
        })
    }
}

export default new HandlerErrors();