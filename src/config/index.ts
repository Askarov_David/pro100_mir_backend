const nconf = require('nconf');
const path = require('path');

class Config {
    public config: any;

    constructor() {
        const configName: string = 'app.config.json';
        this.init(configName);
    }

    init(configName: string) {
        this.config = nconf.argv()
            .env()
            .file({ file: path.join(configName)});
    }
}

module.exports = new Config().config;
