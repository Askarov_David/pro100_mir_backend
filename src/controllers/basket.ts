import { Request, Response, NextFunction } from "express";
import app from '../app';
const jwt = require('jwt-simple');
const config = require('../config');

class BasketController {
    constructor() {}

    public async getAllBasket(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const { page, limit, type } = req.query;
            if(!page && !limit) return next(new Error('400'));
            if (user.permission > 2) return next(new Error('403'));

            let query = {
                offset: page * limit,
                limit: limit,
                order: [
                    ['type', 'DESC']
                ]
            };
            if(type) query['where'] = { type: type.toString().toLowerCase() };

            const basket = await app['db'].basket.findAll(query);
            if(!basket) return next(new Error('400'));
            res.send(basket);
        } catch(error) {
            return next(new Error('500'));
        }
    }

    public async recoveryProfile(req: Request, res: Response, next: NextFunction){
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const { profileId } = req.params;
            if(!profileId || isNaN(profileId)) return next(new Error('400'));
            if (user.permission > 2) return next(new Error('403'));

            const profile = await app['db'].basket.findById(profileId);

            if(!profile) return next(new Error('400'));
            await app['db'][profile.type].create(profile.data);
            await profile.destroy();

            res.json({
                message: 'Профиль был восстановлен'
            });


        } catch(error) {
            return next(new Error('500'))
        }
    }
}

export default new BasketController();