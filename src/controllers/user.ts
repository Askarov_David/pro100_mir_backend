import { Request, Response, NextFunction } from "express";
import { UserAction } from '../shared'
import app from '../app';
import { generateKey, mail } from '../shared';
const jwt = require('jwt-simple');
const config = require('../config');

class UserController {
    constructor(){}

    public async getProfile(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const profile = await app['db'].users.findById(user['userId'], {
                include: [
                    { model: app['db'].roles }
                ]
            });

            if(!profile) return next(new Error('400'));
            res.send(profile);
        } catch (error) {
            return next(new Error('500'));
        }
    }

    public async updateProfile(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            if(req.body['id']) delete req.body['id'];

            req.body['action'] = new UserAction(user.userId, 'Редактирование своего профиля', 'Сотрудники');
            const profile = await app['db'].users.update(req.body, {
                returning: true,
                where: { id: user.userId}
            });
            if(profile['0'] === 0) return next(new Error('400'));

            res.send(profile['1'][0]);
        } catch(error) {
            return next(new Error('500'));
        }
    }

    public async updateProfileById(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const { userId }  = req.params;
            const { role, partner } = req.body;
            if(!userId)  return next(new Error('400'));

            let profile = await app['db'].users.findById(userId, {
                include: { model:  app['db'].roles }
            });
            if(!profile) return next(new Error('400'));

            if(profile.role.permission <= user.permission) return next(new Error('403'));
            req.body['action'] = new UserAction(user.userId, 'Редактирование пользователя', 'Сотрудники');

            delete req.body.id;
            delete req.body.password;
            if(role) req.body.fk_roles_id = role;
            if(partner) req.body.fk_partners_id = user.permission === 1 ? partner : req.query.partnerId;

            profile = await profile.update(req.body);
            if(!profile) next(new Error('500'));

            res.send(profile);
        } catch (error) {
            return next(new Error('500'));
        }
    }

    public async searchThroughProfile(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const term = req.query.term;
            if(!term)  return next(new Error('400'));

            let query = {
                include:[
                    { model: app['db'].roles, attributes: ['id', 'title'] },
                    { model: app['db'].partners, attributes: ['id', 'title'] },
                ],
                where: {
                    $or: [
                        { username: { $like: `%${term}%`} },
                        { email: { $like: `%${term}%`} },
                        { phone: { $like: `%${term}%`} },
                        { "$role.title$": { $like: `%${term}%`} },
                        { "$partner.title$": { $like: `%${term}%`} }
                    ]
                },
                attributes: ['id', 'username', 'phone', 'email', 'key']
            };
            if(user.permission !== 1) {
                query.where['fk_partners_id'] = user.partnerId;
            }
            const users = await app['db'].users.findAll(query);
            res.send(users)
        } catch(error ){
            return next(new Error('500'))
        }
    }

    public async deleteProfileById(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            const { userId }  = req.params;

            const profile = await app['db'].users.findById(userId, {
                include: [
                    { model: app['db'].roles, attributes: ['permission'] },
                    { model: app['db'].partners, attributes: ['id'] }
                ]
            });
            if(!profile) return next(new Error('400'));

            if(user.permission === 1 && profile.role.permission !== 1) {
                if(profile.role.permission === 2) {
                    const count  = await app['db'].users.count({
                        include: { model: app['db'].roles },
                        where: {
                            '$role.permission$': 2
                        }
                    });
                    if(count > 1) {
                        return res.json({
                            message: "Партнер не может функционировать без администратора. Вы удаляете единственного " +
                            " администратора партнера. Создайте еще одного администратора и повторите удаление"
                        })
                    }
                }
                await profile.destroy();
            } else if(user.permission === 2 && profile.role.permission > 2) {
                await profile.destroy();
            } else {
                return next(new Error('403'));
            }
            await app['db'].userAction.create(new UserAction(user.userId, 'Удаление пользователя', 'Сотрудники'));

            res.json({
                message: "Вы успешно удалили профиль " + userId
            });
        } catch (error) {
            return next(new Error('500'));
        }
    }

    public async createProfile(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            if(user.permission > 2) return next(new Error('403'));

            const { username, email, phone, role, partner } = req.body;
            if(!username || !email || !phone || !role || !partner) return next(new Error('400'));

            const body = {
                username,
                email,
                phone,
                fk_roles_id: role,
                fk_partners_id: user.permission === 1 ? partner : req.query.partnerId,
                is_block: false,
                password: generateKey.getKey(),
                key: 'key',
                action: new UserAction(user.userId, 'Создание пользователя', 'Сотрудники')
            };
            const profile = await app['db'].users.create(body);

            if(!profile) return next(new Error('400'));
            delete profile.password;
            delete profile.action;

            const message = `Вы были зарегистрированы на платформу "Просто Мир" \n\r Логин ${email} \n\r Пароль ${body.password}`;
            mail.sendEmail(email, message, 'Платформа "Просто Мир". Предоставление доступа');
            res.send(profile);
        } catch (error) {
            return next(new Error('400'));
        }
    }

    public async deleteProfiles(req: Request, res: Response, next: NextFunction) {
        const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
        const { profiles } = req.body;
        if(!profiles && !Array.isArray(profiles)) return next(new Error('400'));
        if(user.permission !== 1) return next(new Error('403'));
        await app['db'].users.destroy({
            where: {
                $or: {id: profiles }
            }
        });
        await app['db'].userAction.create(new UserAction(user.userId, 'Удаление пользователей', 'Сотрудники'));
        res.json({
            message: "Выбранные профили были успешно удалены"
        });
    }

    public async getActionUser(req: Request, res: Response, next: NextFunction){
        try {
            const { userId }  = req.params;
            const role = req.query.role;
            if( !userId || isNaN(userId) ) return next(new Error('400'));

            let query = {
                include: {
                    model: app['db'].users,
                    attributes: ['username', 'phone', 'email'],
                    include: {
                        model: app['db'].roles,
                        attributes: ['id', 'title']
                    }
                },
                where: {
                    user_id: userId,
                }
            };

            if(role && role && isNaN(role))
                return next(new Error('400'));
            else if(role)
                query.where['$user.role.id$'] = role;

            const actions = await app['db'].userAction.findAll(query);
            if(!actions) return next(new Error('500'));
            res.send(actions);
        } catch(error) {
            return next(new Error('500'));
        }
    }

    public async editActionUserById(req: Request, res: Response, next: NextFunction) {
        try {
            const { actionId }  = req.params;
            if(!actionId || isNaN(actionId)) return next(new Error('400'));
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));

            if(user.permission > 2) return next(new Error('403'));

            const oldAction = await app['db'].userAction.findById(actionId,
                {
                    include: {
                        model: app['db'].users,
                        attributes: ['id', 'fk_partners_id'],
                        include: { model: app['db'].roles }
                    },
                    attributes: ['id']
                });
            if(oldAction.user.role.permission <= user.permission || (oldAction.user.role.permission === 2 && oldAction.user.fk_partners_id !== user.partnerId))
                return next(new Error('403'));


            const action = await app['db'].userAction.update(req.body, {
                returning: true,
                where: { id: user.userId}
            });

            if(!action || action['0'] === 0) return next(new Error('400'));

            res.send(action['1'][0]);
        } catch (error) {
            return next(new Error('500'))
        }
    }

    public async removeActionById(req: Request, res: Response, next: NextFunction){
        try {
            const { actionId }  = req.params;
            if(!actionId || isNaN(actionId)) return next(new Error('400'));
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));

            if(user.permission > 2) return next(new Error('403'));

            const oldAction = await app['db'].userAction.findById(actionId, {
                include: {
                    model: app['db'].users,
                    attributes: ['id', 'fk_partners_id'],
                    include: { model: app['db'].roles }
                },
                attributes: ['id']
            });

            if(oldAction.user.role.permission <= user.permission || (oldAction.user.role.permission === 2 && oldAction.user.fk_partners_id !== user.partnerId))
                return next(new Error('403'));

            await oldAction.destroy();
            res.json({
                message: 'Выбранное событие было успешно удалено'
            })

        } catch (error) {
            return next(new Error('500'))
        }
    }
}

export default new UserController();