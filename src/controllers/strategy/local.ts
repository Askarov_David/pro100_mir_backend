const LocalStrategy = require('passport-local').Strategy;
const jwt = require('jwt-simple');
const passport = require('passport');
const config = require('../../config');
import app from '../../app';

class LocalStorageClass {

    constructor(){
        this.initializeInit();
    }

    public initializeInit() {
        passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        }, async (email, password, done) => {
            const user = await app['db'].users.findOne({
                where: {
                    email,
                    password
                },
                include: [
                    { model: app['db'].roles }
                ]
            });

            if(!user) {
                return done(null, false);
            }
            const token = jwt.encode({
                userId: user.id,
                permission: user.role.permission,
                partnerId: user.fk_partners_id,
                username: user.username
            }, config.get('secret-guide-string'));

            return done(null, {
                id: user.id,
                email: email,
                token: token
            })
        }));
    }
}

export default new LocalStorageClass();

