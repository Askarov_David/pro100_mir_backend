import * as passport from 'passport';
import './local';

class PassportClass {
    constructor(){}

    public init() {
        return passport.initialize()
    }
}

export default new PassportClass();
