import { Request, Response, NextFunction } from "express";
import app from '../app';
import {UserAction} from "../shared";
const jwt = require('jwt-simple');
const config = require('../config');

class PartnersController {
    constructor() {}

    public async getAllPartners(req: Request, res: Response, next: NextFunction) {
        try {
            const partners = await app['db'].partners.findAll();
            res.send(partners)
                .status(200)
        } catch (error) {
            return next(new Error('500'))
        }
    }

    public async createPartner(req: Request, res: Response, next: NextFunction) {
        try {
            const { title, logo }  = req.body;
            if(!title || !logo) return next(new Error('400'));

            const user = jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            if(user.permission !== 1) return next(new Error('403'));

            req.body['action']= new UserAction(user.userId, 'Создание партнера', 'Партнеры');
            const partner = await app['db'].partners.create(req.body);
            res.send(partner);

        } catch (error) {
            next(new Error('400'))
        }
    }

    public async updatePartner(req: Request, res: Response, next: NextFunction) {
        try {
            const { partnerId }  = req.params;
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));

            if(user.permission === 1 || user.permission === 2 && parseInt(partnerId) === user.partnerId ) {
                if(req.body['id']) delete req.body['id'];
                req.body['action']= new UserAction(user.userId, 'Обновление партнера', 'Партнеры');
                const partner = await app['db'].partners.update(req.body, {
                        returning: true,
                        where: { id: partnerId}
                    }
                );

                if(partner['0'] === 0) return next(new Error('400'));

                res.send(partner['1'][0])
            } else {
                return next(new Error('400'));
            }
        } catch (error) {
            next(new Error('400'))
        }
    }

    public async deletePartner(req: Request, res: Response, next: NextFunction) {
        try {
            const { partnerId }  = req.params;
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));

            if(user.permission === 1 ) {
                const partner = await app['db'].partners.findById(partnerId);
                if(!partner) return next(new Error('400'));
                await partner.destroy();

                await app['db'].userAction.create(new UserAction(user.userId, 'Удаление партнера', 'Партнеры'));
                res.json({message: "Вы успешно удалили партнера"});
            } else {
                return next(new Error('403'));
            }

        } catch (error) {
            next(new Error('400'))
        }
    }

    public async getPartner(req: Request, res: Response, next: NextFunction) {
        try {
            const { partnerId }  = req.params;
            if(!partnerId) return next(new Error('400'));
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));
            if(user.permission === 1 || user.permission === 2 && parseInt(partnerId) === user.partnerId ) {
                const partner = await app['db'].partners.findById(partnerId);
                if(!partner) return next(new Error('400'));
                res.send(partner);
            } else {
                return next(new Error('400'));
            }

        } catch(error) {
            next(new Error('400'))
        }
    }

    public async getUsersPartner(req: Request, res: Response, next: NextFunction) {
        try {
            const { partnerId }  = req.params;
            const user = await jwt.decode(req.headers['x-guide-key'], config.get('secret-guide-string'));

            if(user.permission === 1 || user.partnerId === parseInt(partnerId)) {
                const users = await app['db'].users.findAll({
                    include: [
                        { model: app['db'].partners},
                        { model: app['db'].roles, attributes: ['id', 'title']}
                    ],
                    where: {
                        fk_partners_id: partnerId
                    },
                    attributes: ['id', 'username', 'email', 'phone', 'key', 'is_block']
                });

                if(!users) return next(new Error('400'));
                res.send(users);
            } else {
                return next(new Error('403'));
            }
        } catch(error) {
            return next(new Error('500'))
        }
    }
}

export default new PartnersController();