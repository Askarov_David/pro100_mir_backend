import { Request, Response, NextFunction } from "express";
import { mail, generateKey } from '../shared';
import app from '../app';

class AuthorizationController {

    constructor() {}

    public async authorization(req: Request, res: Response, next: NextFunction) {
        try {
            const user = req['user'];
            if(!user) return next(new Error('400'));
            /**
             * IF founded users, you need generate auth key
             */
            const keyAuth = generateKey.getKey();
            const key = await app['db'].authKey.create({
                user_id: user.id,
                secret: keyAuth,
                attempts: 0,
                expire: Date.now() + 1000 * 60 * 60 * 24,
                token: user.token
            });
            if(!key) return next(new Error('500'));
            mail.sendEmail(user.email, keyAuth, 'Платформа "Просто Мир". Ключ для подтверждения авторизации');

            res.status(200);
            res.json({
                userId: user.id,
                message: "Kод подтверждения был отправлен на Ваш email"
            });
        } catch(error) {
            next(new Error('500'));
        }
    }

    public async authConfirmation(req: Request, res: Response, next: NextFunction){
        try {
            const { key, userId } = req.body;
            if(!key || !userId) return next(new Error('400'));

            const secretKey = await app['db'].authKey.findOne({
                where: { user_id: userId },
                include: [
                    {
                        model: app['db'].users,
                        include: [
                            { model: app['db'].roles }
                        ]
                    },
                ]
            });

            if(!secretKey) return next(new Error('400'));
            if((secretKey.date + 1000 * 60 * 60 * 24) <= new Date()) {
                await secretKey.destroy();
                return next(new Error('400'));
            }
            if(secretKey.timeBan && secretKey.timeBan > new Date())  return next(new Error('403'));

            /**
             *  Check the number of attempts to log in
             *  If quantity > 5 turn on the ban for 300 sec
             */
            if(secretKey.secret !== key) {
                secretKey.attempts < 5 ? secretKey.attempts++ : secretKey.attempts = 0;
                secretKey.timeBan = secretKey.attempts === 0 ? Date.now() + (1000 * 60 * 5) : null;
                secretKey.save();
                return next(new Error('400'));
            } else {
                res.header('X-GUIDE-Key', secretKey.token );
            }
            await secretKey.destroy();
            res.send(secretKey);

        } catch(error) {
            next(new Error('400'));
        }
    }

    public async changePassword(req: Request, res: Response, next: NextFunction) {
        try {
            const { email, password } = req.body;
            if(!email || !password) {
                return next(new Error('400'));
            }

            const user = await app['db'].users.findOne({
                where: {
                    email: email
                }
            });
            user.password = password;
            await user.save();
            res.json({
                message: "Пароль был успешно обнавлен"
            }).status(200);
        } catch (error) {
            return next(new Error('500'))
        }
    }

    public async recoveryAccount(req: Request, res: Response, next: NextFunction) {
        try {
            let { partner, message, username, phone, email, role } = req.body;

            if(!partner || !message || !phone || !email || !username || !role) return next(new Error('400'));
            const query = {
                attributes: ['email'],
                where: {
                    fk_roles_id: 2,
                    fk_partners_id: partner
                }
            };
            switch (role) {
                case 'marketolog':
                    query.where.fk_partners_id = 2;
                    break;
                case 'administrator':
                    query.where.fk_partners_id = 1;
                    break;
                default: return next(new Error('400'));
            }

            const admin = await app['db'].users.findAll(query);
            if(!admin) return next(new Error('400'));
            if(admin.length === 0) return res.json({ massage: `На данный момент нет свободного ${role}. Попробуйте позже.`});

            for(let i = 0; i < admin.length; i++) {
                message = `\rПользователь - ${username}. Контактный телефон: ${phone}. Email: ${email} \n\r\n\r 
                           Описание проблемы: \n\r ` + message;
                mail.sendEmail(admin[i].email, message, 'Платформа "Просто Мир". Восстановление доступа');
            }

            res.json({
                message: 'Ваша заявка принята и рассматривается. Вышестоящий администратор свяжется с вами'
            }).status(200);

        } catch(error) {
            return next(new Error('500'));
        }
    }
}

export default new AuthorizationController();