import * as express from "express";
import * as bodyParser from "body-parser";
import * as routes from "./routes";
import { SequelizeDB } from './databases';
import { isAuthMiddleware, handleError } from './middleware';
import passportStrategy from './controllers/strategy';
import { UserRoute } from "./routes/user";

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.init();
    }

    private async init() {
        this.configurate();
        await this.database();
        this.initRoutes();
        this.handlerError();
    }

    private configurate(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use('/static', express.static('src/api.docs/models'));
        this.app.use(passportStrategy.init());
    }

    private initRoutes(): void {
        this.app.use('/doc', new routes.ApiDocumentRoute().routes);
        this.app.use('/api/v1/admin/users', new routes.UserRoute().routes);
        this.app.use('/api/v1/admin/partners', isAuthMiddleware.isAuth, new routes.PartnersRoute().routes);
        this.app.use('/api/v1/admin/basket', isAuthMiddleware.isAuth, new routes.BasketRoute().routes);
    }

    private async database(): Promise<any> {
        return this.app['db'] = await new SequelizeDB().init();
    }

    private handlerError() {
        this.app.use(handleError.errorHandler)
    }
}

export default new App().app;